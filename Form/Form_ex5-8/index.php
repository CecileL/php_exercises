<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form - Exercice 5</title>
</head>
<?php if(!$_POST) {?>
<body>
    <form action="/index.php" method="POST">
        <select name="civilite" id="">
            <option value="Homme">Homme</option>
            <option value="Femme">Femme</option>
        </select>
        <input type="text" name="lastname" id="" placeholder="Nom">
        <input type="text" name="firstname" id="" placeholder="Prénom">
        <input type="file" name="fichier" id="">
        <button>Envoyer</button>
    </form>
<?php } ?>
    <p><?php
        if (isset($_POST['civilite'])) {
            if (isset($_POST['lastname']) && isset($_POST['firstname']) && isset($_POST['fichier'])) {
                if ($_POST['civilite'] == "Homme") {
                    if (pathinfo($_POST['fichier'])['extension'] == "pdf") {
                        echo "Bonjour Monsieur " . $_POST['lastname'] . " " . $_POST['firstname'] . "</br> Votre fichier est : " . $_POST['fichier']; 
                    }else {
                        echo "Votre fichier n'est pas un PDF.";
                    }
                }else if ($_POST['civilite'] == "Femme") {
                    if (pathinfo($_POST['fichier'])['extension'] == "pdf") {
                        echo "Bonjour Monsieur " . $_POST['lastname'] . " " . $_POST['firstname'] . " </br> Votre fichier est : " . $_POST['fichier']; 
                    }else {
                        echo "Votre fichier n'est pas un PDF.";
                    }
                }else {
                    echo "Votre civilité n'est pas renseignée.";
                }
            }else {
                echo "Tous les champs ne sont pas renseignés.";
            }
        }
    ?>
    </p>

</body>
</html>