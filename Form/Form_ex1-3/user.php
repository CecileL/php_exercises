<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form - Exercice 1-3</title>
</head>
<body>
    <?php
        if (isset($_GET['lastname']) && isset($_GET['firstname'])) {
            echo "Bonjour " . $_GET['lastname'] . " " . $_GET['firstname'];
        }else {
            echo "Veuillez renseigner tous les champs.";
        }
    ?>
</body>
</html>