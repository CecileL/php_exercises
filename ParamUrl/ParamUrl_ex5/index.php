<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ParamUrl - Exercice 5</title>
</head>
<body>
    <?php
    if (isset($_GET['semaine'])) {
        echo "Cela fait " . $_GET['semaine'] . " semaines que la formation a commencé.";
    }else {
        echo "Tous les champs ne sont pas renseignés.";
    }
    ?>
</body>
</html>