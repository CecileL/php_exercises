<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ParamUrl - Exercice 6</title>
</head>
<body>
    <?php
        if (isset($_GET['batiment']) && isset($_GET['salle'])) {
            echo "La réunion se passe salle " . $_GET['salle'] . ", batiment " . $_GET['batiment'] . ".";
        }else {
            echo "Tous les champs ne sont pas renseignés !";
        }
    ?>
</body>
</html>