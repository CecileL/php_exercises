<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ParamUrl - Exercice 3</title>
</head>
<body>
    <?php
        if (isset($_GET['dateDebut']) && isset($_GET['dateFin'])) {
            echo "L'évènement commence le " . $_GET['dateDebut'] . " et se termine le " . $_GET['dateFin'] . ".";
        }else {
            echo "Tous les champs ne sont pas renseignés.";
        }
    ?>
</body>
</html>