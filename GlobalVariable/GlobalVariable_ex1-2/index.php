<?php
session_start();

$_SESSION['lastname'] = "Drucker";
$_SESSION['firstname'] = "Michel";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Global Variable - Exercice 1-2</title>
</head>
<body>
    <p><?php 
        echo "User agent : " . $_SERVER['HTTP_USER_AGENT'];
    ?></p>
    <p><?php 
        echo "Adresse IP : " . $_SERVER['REMOTE_ADDR'];
    ?></p>
    <p><?php 
        echo "Nom du serveur : " . $_SERVER['SERVER_NAME'];
    ?></p>

    <a href="/user.php">Le lien de l'espace</a>
</body>
</html>