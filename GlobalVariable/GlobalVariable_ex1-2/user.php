<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Globale Variable - Exercice 1-2</title>
</head>
<body>
    <?php
        echo "Bonjour " . $_SESSION['lastname'] . " " . $_SESSION['firstname'] . ".";
    ?>
</body>
</html>