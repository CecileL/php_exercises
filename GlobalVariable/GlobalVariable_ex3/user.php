<?php
setcookie('login', $_POST['login'], time()+3600*24*31);
setcookie('password', $_POST['password'], time()+3600*24*31);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Globale Variable - Exercice 3-4-5</title>
</head>
<body>
    
    <p>Salut toi !</p>
    <p>
    <?php
        echo "Ton pseudo est " . $_POST['login'] . " et ton mot de passe est " . $_POST['password'] . " .";
    ?>
    </p>
    
</body>
</html>