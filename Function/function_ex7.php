<?php

function personne($age, $genre) {
    if ($age < 18) {
        if ($genre == "homme") {
            return ("Vous êtes un " . $genre . " et vous êtes mineur.");
        }
        else if ($genre == "femme") {
            return ("Vous êtes une " . $genre . " et vous êtes mineur.");
        } else {
            return "Votre genre n'existe pas!";
        }
    }
    else {
        if ($genre == "homme") {
            return ("Vous êtes un " . $genre . " et vous êtes majeur.");
        }
        else if ($genre == "femme") {
            return ("Vous êtes une " . $genre . " et vous êtes majeur.");
        } else {
            return "Votre genre n'existe pas!";
        }
    }
}

echo personne(32, "homme");

?>